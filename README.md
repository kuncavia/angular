## **&lt;ng-phones /&gt;**

### Angular Workshop

Your task is to create an application for a phone store. Below is the sample app.

[SAMPLE APP](https://ng-phones.herokuapp.com)

![Imgur](https://i.imgur.com/bL89xRd.png)

## Step by step guide

1. Create a new application

   - **ng new ng-phones --skip-git=true --skip-tests=true --routing=false --defaults**

1. Install [Angular Material](https://material.angular.io/)

   - **npm install --save @angular/material@7.2.0 @angular/cdk@7.2.0 @angular/animations**

1. In order to make the layout responsive in Angular Material we must use [flex-layout](https://github.com/angular/flex-layout)

   - **npm install @angular/flex-layout@7.0.0-beta.24**
   - Import the **FlexLayoutModule** in the **app.module.ts**
   - [API Flex-layout](https://github.com/angular/flex-layout/wiki/API-Documentation)
   - [Flexbox Guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

1. Open **styles.css** file and put the desired theme there + [Material Icons](https://material.io/icons/)

   - Pick a theme from **node_modules/@angular/material/prebuilt-themes** or use the snippet below
   - [Guide on styles Material Design](https://material.io/guidelines/style/color.html#color-color-palette)

   ```css
   @import '~@angular/material/prebuilt-themes/deeppurple-amber.css';
   @import url('https://fonts.googleapis.com/icon?family=Material+Icons');

   body {
     padding: 20px;
   }
   ```

1. Logo of the application

   - Open the **app.component** and lets add the logo

   ```html
   <div class="logo" fxLayoutGap="15px">
     <i class="material-icons">phonelink_ring</i>
     <span class="logo-txt">&lt;ng-phones /&gt;</span>
   </div>
   ```

   ```css
   .logo {
     font-size: 40px;
   }

   .logo i {
     font-size: 40px;
     vertical-align: middle;
   }

   .logo-txt {
     vertical-align: middle;
   }
   ```

### Lets get to work!

1. Lets create the models first

   - **ng g interface models/phone**
   - **ng g interface models/specification**
   - **ng g enum models/display-type**

   ```ts
   export interface Phone {
     brand: string;

     brandImg: string;

     model: string;

     description: string;

     imgUrl: string;

     price: number;

     specs: Specification;
   }
   ```

   ```ts
   export enum DisplayType {
     LED,
     LCD,
     AMOLED
   }
   ```

   ```ts
   export interface Specification {
     displayType: DisplayType;

     displaySize: number;
   }
   ```

1. In the **app.component.ts** file make a collection of phones

   ```ts
   public phones: Phone[] = [
       { brand: 'Samsung', brandImg: 'https://seeklogo.com/images/S/Samsung_Mobile-logo-D8645D09B2-seeklogo.com.png', description: 'Best camera ever', model: 'S9+', price: 899, imgUrl: 'https://staticshop.o2.co.uk/product/images/samsung_galaxy_s9_64gb_black_sku_header.png?cb=2d5f3cefdd7ecc08b3aa71c58896c0a3', specs: { displaySize: 5.8, displayType: DisplayType.AMOLED } },
       { brand: 'OnePlus', brandImg: 'https://seeklogo.com/images/O/oneplus-logo-B6703954CF-seeklogo.com.png', description: 'Bang for the bucks', model: '5T', price: 599, imgUrl: 'https://staticshop.o2.co.uk/product/images/oneplus-5t-sku-header.png?cb=a1c2633b92f841bdf825d1c718eec321', specs: { displaySize: 6.0, displayType: DisplayType.AMOLED } },
       { brand: 'iPhone', brandImg: 'https://www.pchouselondon.com/wp-content/uploads/2018/02/apple.png', description: 'Notch', model: 'X', price: 999, imgUrl: 'https://cdn.macrumors.com/article-new/2017/10/iphone-x-silver.jpg', specs: { displaySize: 5.8, displayType: DisplayType.AMOLED } },
   ];
   ```

1. It is time to create our first component now. It must be a **phone-list** component

   - **ng g c phones/phone-list --no-spec=true**

1. The **phone-list.component.ts** file must have a field **phones: Phone[]**

   - In order to get the phones the filed must be **@Input()**

   ```ts
   @Input()
   public phones: Phone[];
   ```

1. The **phone-list.component.html** should display the phones array

   ```html
   <div fxLayout="row wrap" fxLayoutAlign="start start" fxLayoutGap="20px">
     <div fxFlex="20%" class="card-picture" *ngFor="let phone of phones">
       {{phone.brand}} {{phone.model}}
     </div>
   </div>
   ```

1. In **app.component.html** file place the **app-phone-list** selector

   ```html
   <app-phone-list [phones]="phones"></app-phone-list>
   ```

1. Now lets create another component which will display each phone in the phones array

   - **ng g c phones/phone-view --no-spec=true**

1. Now the **phone-view.component.ts** must have an input property for the **Phone**

   - Following the example from the phone-list component create one here

1. In the html file of **phone-view** create a **Material Card** to display the phone

   - example: https://material.angular.io/components/card/examples

   ```html
   <mat-card>
     <mat-card-header>
       <mat-card-title>Brand: {{phone.brand}}</mat-card-title>
       <mat-card-subtitle>Model: {{phone.model}}</mat-card-subtitle>
     </mat-card-header>
     <div class="img-phone" fxLayout="row">
       <img
         fxFlexAlign="center"
         mat-card-image
         [src]="phone.imgUrl"
         alt="{{phone.brand}}-{{phone.model}}"
       />
     </div>
     <mat-card-content>
       <p class="price-value">
         Price: ${{phone.price}}
       </p>
     </mat-card-content>
   </mat-card>
   ```

   ```css
   mat-card {
     max-width: 400px;
     margin: 20px;
   }

   .img-phone {
     width: 100%;
   }

   mat-card-title {
     font-weight: bold;
   }

   .price-value {
     font-style: italic;
   }
   ```

1. Now there must be an error when compiling

   - You are missing some modules
   - Open **app.module.ts**
   - Import **FormsModule** in the imports array (this has nothing to do with Angular Material. It is Angular module for working with forms.)
   - In order to use Angular Material cards you must import **MatCardModule** as well.

1. Now lets use the **phone-view** component in the **phone-list** component

   ```html
   <div fxLayout="row wrap" fxLayoutAlign="start start" fxLayoutGap="20px">
     <div fxFlex="20%" class="card-picture" *ngFor="let phone of phones">
       <app-phone-view [phone]="phone"></app-phone-view>
     </div>
   </div>
   ```

1. Lets create select dropdown to order phones by price, model or brand

   - open **app.component.html**

   ```html
   <div fxLayout="row wrap" fxLayoutGap="15px">
     <mat-form-field fxFlex="20%">
       <mat-select
         (selectionChange)="sortByType()"
         [(ngModel)]="order"
         placeholder="Select Order"
       >
         <mat-option value="asc">Ascending</mat-option>
         <mat-option value="desc">Descending</mat-option>
       </mat-select>
     </mat-form-field>
     <mat-form-field fxFlex="20%">
       <mat-select
         (selectionChange)="sortByType()"
         [(ngModel)]="byType"
         placeholder="Select Type"
       >
         <mat-option value="brand">Brand</mat-option>
         <mat-option value="price">Price</mat-option>
         <mat-option value="model">Model</mat-option>
       </mat-select>
     </mat-form-field>
   </div>
   ```

1. Another error when compiling...

   - Add **MatSelectModule** and **BrowserAnimationsModule** in **app.module.ts**
   - Try now

1. Look at the html used above

   - We have two properties bound to **[(ngModel)]** - **order** and **byType**
   - Now we need to declare them in the component.ts file
   - **(selectionChange)="sortByType()"** is an event binding which will call the **sortByType()** method on every change of the select dropdown
   - implement sorting in this method in the **app.component.ts** file

   ```ts
   sortByType(): void {
       // implement it...
       this.filteredPhones = this.filteredPhones.sort((x, y)=>...
   }
   ```

   - **HINT:** In order to not modify the phones collection use new collection called ex: **filteredPhones: Phone[]**
   - Use **ngOnInit()** method to initialize it
   - https://angular.io/api/core/OnInit

   ```ts
   ngOnInit(): void {
     this.filteredPhones = this.phones.sort((x, y) => x.brand.localeCompare(y.brand));
   }
   ```

   - The above snippet will create the collection on component initialization
   - Move to **app.component.html** and change

   ```html
   // change this
   <app-phone-list [phones]="phones"></app-phone-list>
   // to this
   <app-phone-list [phones]="filteredPhones"></app-phone-list>
   ```

1. Lets make the phones searchable

   - Open **app.component.html** again and use the template below

   ```html
   <mat-form-field fxFlex="40%">
     <input
       matInput
       placeholder="Search"
       (input)="filterBySearch()"
       [(ngModel)]="search"
     />
   </mat-form-field>
   ```

   - Now you have to include another module - **MatInputModule** in the **app.module.ts** file
   - Add the property search in the component.ts file
   - Implement the method **filterBySearch()**. The snippet below could be used.

   ```ts
   this.search = this.search.toLowerCase().trim();

   this.filteredPhones = this.phones.filter(
     x =>
       x.brand.toLowerCase().indexOf(this.search) >= 0 ||
       x.description.toLowerCase().indexOf(this.search) >= 0 ||
       x.model.toLowerCase().indexOf(this.search) >= 0
   );
   ```

1. Now it is time to show some details about each phone

   - Create a new component **ng g c phones/phone-details --no-spec=true**
   - Use the template below

   ```html
   <div fxLayout="row" fxLayoutAlign="center center">
     <mat-card>
       <mat-card-header fxLayout="row">
         <div fxFlex="75">
           <mat-card-title>Brand: {{phone.brand}}</mat-card-title>
           <mat-card-subtitle>Model: {{phone.model}}</mat-card-subtitle>
         </div>
         <div fxFlex="25">
           <img class="brand-img" [src]="phone.brandImg" alt="brand-image" />
         </div>
       </mat-card-header>
       <div class="img-phone" fxLayout="row wrap" fxLayoutGap="30px">
         <img
           fxFlexAlign="start"
           fxFlex="40"
           mat-card-image
           [src]="phone.imgUrl"
           alt="{{phone.brand}}-{{phone.model}}"
         />
         <div fxFlex="45" fxFlexAlign="start" fxLayout="column">
           <p>Specifications:</p>
           <p fxFlex="25">
             DisplayType: {{DisplayType[phone.specs.displayType]}}
           </p>
           <p fxFlex="25">
             DisplaySize: {{phone.specs.displaySize}} "
           </p>
         </div>
         <div class="desc-phone" fxFlex="100">
           Description: {{phone.description}}
         </div>
       </div>
       <mat-card-content>
         <p class="price-value">
           Price: ${{phone.price}}
         </p>
       </mat-card-content>
       <mat-card-actions>
         <button class="close-btn" mat-raised-button>
           Close
         </button>
       </mat-card-actions>
     </mat-card>
   </div>
   ```

   ```css
   mat-card {
     position: absolute;
     top: 50px;
     max-width: 400px;
     margin: 20px;
     position: fixed;
   }

   .img-phone {
     margin-top: 20px;
   }

   mat-card-title,
   mat-card-subtitle {
     font-weight: bold !important;
     font-size: 20px !important;
   }

   .price-value {
     font-style: italic;
   }

   .desc-phone {
     margin: 15px 0;
   }

   .brand-img {
     width: 70%;
   }

   .close-btn {
     background-color: dodgerblue;
     color: white;
   }
   ```

   - Get the **Phone** from an **@Input** property
   - In order to use DisplayType in the template go to the **app-phone-details.component.ts** and declare **DisplayType = DisplayType;** property
   - Import the **MatButtonModule** in **app.module.ts** to be able to use the [Material buttons](https://material.angular.io/components/button/overview)

1. Now lets show the phone details

   - Make an **@Output()** decorator to emit the event from **phone-view.component.ts**

   ```ts
   @Output()
   showPhone = new EventEmitter();
   ```

   ```ts
   onPhoneClick(): void {
       this.showPhone.emit(this.phone);
   }
   ```

   - In the **phone-view.component.html** catch the click event on the whole card element and call **onPhoneClick()** method to notify the parent component
   - Catch the emitted event in the **phone-list.component.html**

   ```html
   <app-phone-view
     (showPhone)="showPhone($event)"
     [phone]="phone"
   ></app-phone-view>
   ```

   - Now in the **phone-list.component.ts** file implement the **showPhone()** handler

   ```ts
   public currentPhone: Phone;

   // here phone argument is actually passed with $event property
   showPhone(phone: Phone) {
     this.currentPhone = phone;
   }
   ```

   - In the html file add the new **phone-details.component**

   ```html
   <div *ngIf="currentPhone">
     <app-phone-details [phone]="currentPhone"></app-phone-details>
   </div>
   ```

1. Now if everything is ready we must hide the phone details

   - Add an event binding on click to the close button element in the **phone-details.component.html**

   ```html
   <button class="close-btn" mat-raised-button (click)="onCloseButtonClick()">
     Close
   </button>
   ```

   - Implement the **onCloseButtonClick()** and an **@Output()** property in **phone-details.component.ts** file

   ```ts
    @Output()
    hidePhone = new EventEmitter();

    onCloseButtonClick(): void {
      this.hidePhone.emit(this.phone);
    }
   ```

   - Catch the event in the parent **phone-list.component.html**

   ```html
   <app-phone-details
     (hidePhone)="hidePhone()"
     [phone]="currentPhone"
   ></app-phone-details>
   ```

   - Implement the **hidePhone()** method in **phone-list.component.ts**

   ```ts
   hidePhone() {
       this.currentPhone = undefined;
   }
   ```

1. Lets make it close not only on click on the button, but even if we click outside of the card

   - **npm install ng-click-outside --save**
   - Include the **ClickOutsideModule** in the modules in **app.module.ts**

   ```html
   <app-phone-details
     (clickOutside)="hidePhone()"
     (hidePhone)="hidePhone()"
     [phone]="currentPhone"
   ></app-phone-details>
   ```

   - Use the same **hidePhone()** method but this time with the **(clickOutside) event** declared by the installed package
   - In order to bypass the trigger of the **clickOutside** event when you initialy click to show a given phone, change the **showPhone()** method to the snippet below
  
   ```ts
   showPhone(phone: Phone) {
     setTimeout(() => this.currentPhone = phone, 10);
   }
   ```
   
   - Try it without the **setTimeout** and find out why it isn't working

1. Implement Clear Filters button in the **app.component**

   ```html
   <button mat-raised-button (click)="clearFilters()">Clear filters</button>
   ```

   ```ts
   clearFilters() {
    // implement it
   }
   ```
