import { Component } from '@angular/core';
import { Phone } from './models/phone';
import { DisplayType } from './models/display-type.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ng-phones';
  public byType: string | number;
  public order: string;
  public filteredPhones: Phone[];
  public search: string;

  public phones: Phone[] = [
    {
      brand: 'Samsung', brandImg: 'https://seeklogo.com/images/S/Samsung_Mobile-logo-D8645D09B2-seeklogo.com.png',
      description: 'Best camera ever', model: 'S9+', price: 899, imgUrl:
        'https://staticshop.o2.co.uk/product/images/samsung_galaxy_s9_64gb_black_sku_header.png?cb=2d5f3cefdd7ecc08b3aa71c58896c0a3',
      specs: { displaySize: 5.8, displayType: DisplayType.AMOLED }
    },
    {
      brand: 'OnePlus', brandImg: 'https://seeklogo.com/images/O/oneplus-logo-B6703954CF-seeklogo.com.png',
      description: 'Bang for the bucks', model: '5T', price: 599, imgUrl:
        'https://staticshop.o2.co.uk/product/images/oneplus-5t-sku-header.png?cb=a1c2633b92f841bdf825d1c718eec321',
      specs: { displaySize: 6.0, displayType: DisplayType.AMOLED }
    },
    {
      brand: 'iPhone', brandImg: 'https://www.pchouselondon.com/wp-content/uploads/2018/02/apple.png',
      description: 'Notch', model: 'X', price: 999, imgUrl:
        'https://cdn.macrumors.com/article-new/2017/10/iphone-x-silver.jpg',
      specs: { displaySize: 5.8, displayType: DisplayType.AMOLED }
    },
  ];

  ngOnInit(): void {
    this.filteredPhones = this.phones.sort((x, y) => x.brand.localeCompare(y.brand));
  }

  sortByType(): void {
    if (this.byType === 'brand' && this.order === 'desc') {
      this.filteredPhones = this.filteredPhones.sort((x, y) => y.brand.localeCompare(x.brand));
    }
    if (this.byType === 'brand' && this.order === 'asc') {
      this.filteredPhones = this.filteredPhones.sort((x, y) => x.brand.localeCompare(y.brand));
    }
    if (this.byType === 'price' && this.order === 'desc') {
      this.filteredPhones = this.filteredPhones.sort((x, y) => y.price - x.price);
    }
    if (this.byType === 'price' && this.order === 'asc') {
      this.filteredPhones = this.filteredPhones.sort((x, y) => x.price - y.price);
    }
    if (this.byType === 'model' && this.order === 'desc') {
      this.filteredPhones = this.filteredPhones.sort((x, y) => y.model.localeCompare(x.model));
    }
    if (this.byType === 'model' && this.order === 'asc') {
      this.filteredPhones = this.filteredPhones.sort((x, y) => x.model.localeCompare(y.model));
    }
  }
  filterBySearch() {
    this.search = this.search.toLowerCase().trim();

    this.filteredPhones = this.phones.filter(x =>
      x.brand.toLowerCase().indexOf(this.search) >= 0 ||
      x.description.toLowerCase().indexOf(this.search) >= 0 ||
      x.model.toLowerCase().indexOf(this.search) >= 0
    );
  }
}
