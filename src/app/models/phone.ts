import { Specification } from './speification';

export interface Phone {
    brand: string;

    brandImg: string;

    model: string;

    description: string;

    imgUrl: string;

    price: number;

    specs: Specification;
}
