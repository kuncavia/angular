import { Component, OnInit, Input } from '@angular/core';
import { Phone } from 'src/app/models/phone';

@Component({
  selector: 'app-phone-list',
  templateUrl: './phone-list.component.html',
  styleUrls: ['./phone-list.component.css']
})
export class PhoneListComponent {
  @Input()
  public phones: Phone[];
  public currentPhone: Phone;

  constructor() { }

  // here phone argument is actually passed with $event property
  showPhone(phone: Phone) {
    this.currentPhone = phone;
  }

  hidePhone() {
    this.currentPhone = undefined;
}


}
