import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Phone } from 'src/app/models/phone';
import { DisplayType } from 'src/app/models/display-type.enum';

@Component({
  selector: 'app-phone-details',
  templateUrl: './phone-details.component.html',
  styleUrls: ['./phone-details.component.css']
})
export class PhoneDetailsComponent {
  @Input()
  public phone: Phone;
  public DisplayType = DisplayType;

  @Output()
  public hidePhone = new EventEmitter();

  constructor() { }

  onCloseButtonClick(): void {
    this.hidePhone.emit(this.phone);
  }
}
