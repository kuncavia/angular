import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppComponent } from './app.component';
import { PhoneListComponent } from './phones/phone-list/phone-list.component';
import { PhoneViewComponent } from './phones/phone-view/phone-view.component';
import { FormsModule } from '@angular/forms';
import { MatCardModule, MatSelectModule, MatInputModule, MatButtonModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PhoneDetailsComponent } from './phones/phone-details/phone-details.component';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
  declarations: [
    AppComponent,
    PhoneListComponent,
    PhoneViewComponent,
    PhoneDetailsComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    FormsModule,
    MatCardModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    ClickOutsideModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
